import React, { Component } from "react";
import { render } from "react-dom";
import CalculatorFrom from "./containers/CalculatorFrom";

class App extends Component {
  render() {
    return (
        <CalculatorFrom />
    );
  }
}

render(<App />, document.getElementById("root"));
