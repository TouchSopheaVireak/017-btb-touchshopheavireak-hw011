import React, { Component } from "react";
import {Card, Col} from 'react-bootstrap';
/* Import Components */
import Input from "../components/Input";
import Select from "../components/Select";
import Button from "../components/Button";

class CalculatorFrom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      calculatorJson: {
        first_number: "",
        second_number: "",
        operator: "",
        result: []
      },

      operatorOptions: ["+ Plus", "- Subtract", "x Multiply", "/ Divide", "% Module"],
    };

    this.handleFirstNumber = this.handleFirstNumber.bind(this);
    this.handleSecondNumber = this.handleSecondNumber.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  handleFirstNumber(e) {
    let value = e.target.value;
    this.setState (
      prevState => ({
        calculatorJson: {
          ...prevState.calculatorJson,
          first_number: value
        }
      }),
    );
  }

  handleSecondNumber(e) {
    let value = e.target.value;
    this.setState (
      prevState => ({
        calculatorJson: {
          ...prevState.calculatorJson,
          second_number: value
        }
      }),
    );
  }

  handleInput(e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => ({
        calculatorJson: {
          ...prevState.calculatorJson,
          [name]: value
        }
      }),
    );
  }
  
  handleFormSubmit(e) {
    e.preventDefault();
    let calResult = 0;
    let calObj = this.state.calculatorJson;
    if ((calObj.first_number || calObj.second_number) < 0) {
        alert("invalid input value !!!");
    } else {
        if (calObj.operator === "+ Plus") {
            calResult = parseInt(calObj.first_number) + parseInt(calObj.second_number);
        } else if (calObj.operator === "- Subtract") {
            calResult = parseInt(calObj.first_number) - parseInt(calObj.second_number);
        } else if (calObj.operator === "x Multiply") {
            calResult = parseInt(calObj.first_number) * parseInt(calObj.second_number);
        } else if (calObj.operator === "/ Divide") {
            calResult = parseInt(calObj.first_number) / parseInt(calObj.second_number);
        } else if (calObj.operator === "% Module") {
            calResult = parseInt(calObj.first_number) % parseInt(calObj.second_number);
        } else {
            alert("invalid operator value !!!");
        }
    }

    // inserData to result[]
    this.setState (
      prevState => ({
        calculatorJson: {
          ...prevState.calculatorJson,
          result: calObj.result.concat([calResult])
        }
      }),
      () => console.log(this.state.calculatorJson)
    );
  }
  
  // clear
  handleClearForm(e) {
    e.preventDefault();
    this.setState ({
      calculatorJson: {
        first_number: "",
        second_number: "",
        operator: "",
        result: []
      }
    });
  }

  render() {
    return (
        <div className="container" style = {{marginTop: "5%",  marginBottom: "3%"}}>
            <div className="row">
              <Col ms = "6">
                <Card>
                  <Card.Img variant="top" src="51xa4WtV1wL.png" />
                  <Card.Body>
                    <form className="container-fluid" onSubmit = {this.handleFormSubmit}>
                      <Input
                        inputType = {"number"}
                        name = {"first_number"}
                        title = {""}
                        value = {this.state.calculatorJson.first_number}
                        placeholder = {"Number"}
                        handleChange = {this.handleFirstNumber}
                      />
                      {/* First Number of the user */}

                      <Input
                        inputType = {"number"}
                        name = {"second_number"}
                        title = {""}
                        value = {this.state.calculatorJson.second_number}
                        placeholder = {"Number"}
                        handleChange = {this.handleSecondNumber}
                      />
                      {/* second Number of the user */}

                      <Select
                        title = {""}
                        name = {"operator"}
                        options = {this.state.operatorOptions}
                        value = {this.state.calculatorJson.operator}
                        placeholder = {"Select Operator"}
                        handleChange = {this.handleInput}
                      />
                      {/* select Option for the user */}

                      <Button
                        action = {this.handleFormSubmit}
                        type = {"primary"}
                        title = {"Calculator"}
                        style = {buttonStyle}
                      />
                      {/*Calculator */}

                      <Button
                        action = {this.handleClearForm}
                        type = {"secondary"}
                        title = {"Clear"}
                        style = {buttonStyle}
                      />
                      {/* Clear the form */}

                    </form>
                  </Card.Body>
                </Card>    
              </Col>

              <Col ms = "6">
                <h3> Result History </h3>
                 {/* Result calculator */}
                <ul className="list-group">
                  {this.state.calculatorJson.result.map(function(item) {
                    return <li key = {item} class="list-group-item">
                      <a href="#" class="text-primary">{item}</a>
                    </li>;
                  })}
                </ul>
              </Col>
            </div>
        </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

export default CalculatorFrom;
